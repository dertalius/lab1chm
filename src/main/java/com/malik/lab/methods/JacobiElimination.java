package com.malik.lab.methods;

public class JacobiElimination {

    public static void solve(int n, double[][] A, double[] F, double[] X) {
        double[] tempX = new double[n];
        double norm;

        do {
            for (int i = 0; i < n; i++) {
                tempX[i] = F[i];
                for (int g = 0; g < n; g++) {
                    if (i != g)
                        tempX[i] -= A[i][g] * X[g];
                }
                tempX[i] /= A[i][i];
            }

            norm = Math.abs(X[0] - tempX[0]);
            for (int h = 0; h < n; h++) {
                if (Math.abs(X[h] - tempX[h]) > norm)
                    norm = Math.abs(X[h] - tempX[h]);
                X[h] = tempX[h];
            }
        } while (norm > 0.001);
    }
}
