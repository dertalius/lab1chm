<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Руководство пользователя</title>
</head>
<body>
Руководство пользователя
<p>
    Спершу потрібно зайти на головну сторінку.<br/> Необхідно обрати яким чином ввести дані. Якщо обрано спосіб вручну, необхідно обрати кількість змінних, та натиснути кнопку «Далі». Потім ввести відповідні параметри (матриці А та В), обрати метод у відповідному полі та натиснути кнопку для знаходження результатів. <br/>Зрештою буде показано результати роботи алгоритму та буде запропоновано зберегти звіт з роботи програми, а також введені вхідні дані.
</p>
<p>Якщо обрано зчитування з файлу, потрібно обрати файл та натиснути кнопку «Завантажити». Файл буде завантажено та показано уже заповнені поля відповідними даними. Далі потрібно обрати метод простих ітерацій та натиснути кнопку. Результат буде такий самий, як і при введенні даних вручну.
</p>

<h3>Department`s list</h3>
<table border="1">
    <thead>
    <tr>
        <th>ID</th>
        <th>name</th>
        <th>surname</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="department" items="${list}" >
        <tr>
            <td>${department.id}</td>
            <td>${department.name}</td>
            <td>${department.surname}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
