<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
  <head>
    <title>Начальная страница</title>
  </head>
  <body>
  <div>
    <ul>
      <li><a target="_blank" href="https://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4_%D0%93%D0%B0%D1%83%D1%81%D1%81%D0%B0" >Сведения о методе Гаусса.</a></li>
        <li><a target="_blank" href="https://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4_%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B9_%D0%B8%D1%82%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8">Сведения о методе простых итераций.</a></li>
        <li><a href="${pageContext.request.contextPath}/help">Руковдоство пользователя.</a></li>
    </ul>
  </div>
  <div>
      <p>Выберите количество переменных:</p>
    <form:form action="/tomethod" method="post">
        <select name="variablesCount" >
            <option value="3" >3</option>
            <option value="4" >4</option>
            <option value="5" >5</option>
            <option value="6" >6</option>
            <option value="7" >7</option>
        </select>
        <input type="submit" value="Дальше">
    </form:form>
  </div>
  <div>
      <p>Или загрузите файл с готовыми данными</p>
      <form method="POST" action="uploadFile" enctype="multipart/form-data">
          Выберите файл:
          <input type="file" name="file">
          Нажмите, чтоб загрузить!
          <input type="submit" value="Загрузить">
      </form>
  </div>

  </body>
</html>
