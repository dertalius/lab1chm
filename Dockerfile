FROM openjdk:8
RUN apt-get update && apt-get install git -y
RUN git clone https://github.com/vanilmalik/lab1chm.git
#RUN git clone https://ivan_malik@bitbucket.org/ivan_malik/learning-servlets.git
RUN apt-get update -y && apt-get install maven -y
WORKDIR lab1chm
#WORKDIR learning-servlets
RUN mvn clean install 

FROM tomcat:8.0.20-jre8
COPY --from=0 lab1chm/target/Lab1.war /usr/local/tomcat/webapps/Lab1.war
#COPY --from=0 learning-servlets/target/learning-servlets-2.war /usr/local/tomcat/webapps/learning-servlets-2.wars
